<b>Beginnen</b>

Entweder die installer.sh ausführen oder an der Anleitung orientieren.
##
<b>Docker installieren</b>

sudo apt install docker.io docker-compose
###
sudo groupadd docker
####
sudo usermod -aG docker $USER
####
systemctl reboot
<!-- ###
Falls Apache2 oder eine Datenbankanwendung laufen müssen diese deaktiviert werden Bsp.:

systemctl stop mariadb.service

systemctl stop apache2.service -->
##
<b>Projekt starten</b>

docker-compose up -d

<b>Projekt beenden</b>

docker-compose down -v
####
<b>Achtung!</b> Wenn das Projekt beendet wird gehen die Daten verloren.

Bevor das Projekt beendet wird, um die Daten zu sichern:

mysqldump -h127.0.0.1 -uroot -proot -P3307 nachweis_db > eigenedaten.sql

Nachdem das Projekt neu gestartet wurde:

mysql -h127.0.0.1 -uroot -proot -P3307

Und die Daten eintragen:

use nachweis_db;

source eigenedaten.sql;

##
<b>Projekt ansehen</b>

http://localhost:5001/ausbildungsnachweis/

##
<b>Bemerkung</b>

1. Strg+P für PDF.
2. Wenn auf Fedora der Zugriff auf die Seite verweigert wird dann sollte
die Option "SELINUX" in der /etc/selinux/config auf "permissive" geändert werden.
